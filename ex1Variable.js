//javascript basic Example by Nipu Chakraborty
//javascript output method for show result 

//window.alert("Hello! Nipu");//it's also can used just aleart
console.log("Hello Nipu!");
document.write("Hello Nipu");
document.writeln("Hello, Nipu");
print("Hello Nipu");




//var is an  reseved key word i javascript
//let is an  reseved key word i javascript
//const is an new fetures keyword in ES 6 by js
//syntex
var variableNameForVAr = "variable value as you like assign here";
let variableNameForLet = "varable value as you like assign here"
const variableNameForConst="This value Can't be changed because this is an constant value"
// varible declearation in js

var name = "Nipu Chakraborty";
console.log("yeah! , your Name is  =>"+name);


let fisrtName = "Nipu";
let lastName = "Chakraborty";
let fullName = fisrtName + " " + lastName;
console.log("yeah ! your Full Name is form let keyword=>" + fullName);


const unchangedFisrstName = "Nipu";
const unchangedLastName = "Chakraborty";
const unchangedFullName = unchangedFisrstName +" "+ unchangedLastName;
console.log("Yeah ! your unchaged full Name is =>"+unchangedFullName);

// in javascript you decleat defferent type of value 
//in var let const
// let's take an example for var  
var integerNumber = 100;
var floatNumber = 1.444;
var StringValue = "Hi, this an string Value";
var boolenValue = true;
console.log(integerNumber + " " + floatNumber + " " + StringValue + " " + boolenValue);
//let
let integerNumberInlet = 200;
let floatNumberInlet = 51.444;
let StringValueInlet = "Hi, this an string Value";
let boolenValueIlet;
boolenValueIlet = true;
console.log(integerNumberInlet + " " + floatNumberInlet + " " + StringValueInlet + " " + boolenValueIlet);
//const
const YouCanNotChangeIntegerNumber = 100;
const YouCanNotChangeFloatNumber = 1.444;
const YouCanNotChangeStringValue = "Hi, this an string Value";
const YouCanNotChangeboolenValue = true;
console.log(YouCanNotChangeIntegerNumber + " " + YouCanNotChangeFloatNumber + " " + YouCanNotChangeStringValue + " " + YouCanNotChangeboolenValue);

//Ex: 1

/* The Fortune Teller
Why pay a fortune teller when you can just program your fortune yourself ?

solution:
Store the following into variables : number of children, partner 's name, geographic location, job title.
Output your fortune to the screen like so: "You will be a X in Y, and married to Z with N kids." */

var numKids = 5;
var partner = 'David Beckham';
var geoLocation = 'Costa Rica';
var jobTitle = 'web developer';

var future = 'You will be a ' + jobTitle + ' in ' + geoLocation + ', and married to ' +
    partner + ' ' + ' with ' + numKids + ' kids.';
console.log(future);


//Ex:2
/* The Age Calculator
Want to find out how old you 'll be? Calculate it!

Store your birth year in a variable.
Store a future year in a variable.
Calculate your 2 possible ages
for that year based on the stored values.
For example,
if you were born in 1988, then in 2026 you 'll be either 37 or 38, depending on what month it is in 2026.
Output them to the screen like so: "I will be either NN or NN in YYYY", substituting the values. */
//solution:

var birthYear = 1984;
var futureYear = 2012;
var age = futureYear - birthYear;
console.log('I will be either ' + age + ' or ' + (age - 1));


//Ex:3
/* The Lifetime Supply Calculator
Ever wonder how much a "lifetime supply" of your favorite snack is ? Wonder no more!

    Store your current age into a variable.
Store a maximum age into a variable.
Store an estimated amount per day(as a number).
Calculate how many you would eat total
for the rest of your life.
Output the result to the screen like so: "You will need NN to last you until the ripe old age of X". */
//solution
var age = 28;
var maxAge = 100;
var numPerDay = 2;
var totalNeeded = (numPerDay * 365) * (maxAge - age);
var message = 'You will need ' + totalNeeded + ' cups of tea to last you until the ripe old age of ' + maxAge;
console.log(message);


//Ex:4
/* The Geometrizer
Calculate properties of a circle, using the definitions here.

Store a radius into a variable.
Calculate the circumference based on the radius, and output "The circumference is NN".
Calculate the area based on the radius, and output "The area is NN". */
//soution
var radius = 3;
var circumference = Math.PI * 2 * radius;
console.log("The circumference is " + circumference);
var area = Math.PI * radius * radius;
console.log("The area is " + area);

//ex:5

/* It 's hot out! Let'
s make a converter based on the steps here.

Store a celsius temperature into a variable.
Convert it to fahrenheit and output "NN°C is NN°F".
Now store a fahrenheit temperature into a variable.
Convert it to celsius and output "NN°F is NN°C." */
//solution

var celsius = 30;
var celsiusInF = (celsius * 9) / 5 + 32;
console.log(celsius + '°C is ' + celsiusInF + '°F');
var fahrenheit = 20;
var fahrenheitInC = ((fahrenheit - 32) * 5) / 9;
console.log(fahrenheit + '°F is ' + fahrenheitInC + '°C');



//Oparator Example 
//Arithmethic oparators

//pluse oparator

let value1 = 100;
let value2 = 300;
let sum = value1 + value2;
console.log(sum);

//subtractor oparator
let sub = value1 - value2;
console.log(sub);
//Multiplication oparator
let mul = value1 * value2;
console.log(mul);
//divide oparator
let div = value1 / value2;
console.log(div);


//logical oparator

//logical and oparator

let yourMassage = "Hello ,Good morning sir!";
let sirReplyThat = "Good Morning Nipu";


if (yourMassage === "Hello ,Good morning sir!" && sirReplyThat === "Good Morning Nipu") {
    console.log("Ow Thank you sir, have a good day");  
}

//logical or oparator
let youHaveValue1 = 100;
let youHaveValue2 = 200;
if (youHaveValue1 === 100 || youHaveValue2 == 300) {
    console.log('You have a value Nipu');  
}

//logical not 

let sayHi = "Hi!";
if (!sayHi==="Hi!") {
    console.log("I didn't here sir!");
}
else {
    console.log('oh Hi!');
}

//Practtice by this

/* Assignment x = y x = y
Addition assignment x += y x = x + y
Subtraction assignment x -= y x = x - y
Multiplication assignment x *= y x = x * y
Division assignment x /= y x = x / y
Remainder assignment x %= y x = x % y
Exponentiation assignment x **= y x = x ** y
Left shift assignment x <<= y x = x << y
Right shift assignment x >>= y x = x >> y
Unsigned right shift assignment x >>>= y x = x >>> y
Bitwise AND assignment x &= y x = x & y
Bitwise XOR assignment x ^= y x = x ^ y
Bitwise OR assignment x |= y x = x | y
Assignment */
