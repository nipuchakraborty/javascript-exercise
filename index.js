/* const and let */
const name = "Nipu Chakraborty"

//name = 10;//con't be postible

console.log(name);


let name2 = "Nipu Chakraborty";
console.log(name2);

/* objects  */

let myInfo = {
  name: "Nipu Chakraborty",
  age:"21"
}

//object data passing with key
myInfo.address = "Changraghona";
console.log(myInfo);

/* regular function */

function person() {
  console.log("I a regular function I think got it"); 
}

person();

/* Arrow function */

const person2 = () => {
  console.log("I am a Arrow function I think got it ");
}

person2();
 
/* arrao function with params */
const arrowFunctionWithParam = (name, age, address) => {
  console.log(name);
  console.log(age);
  console.log(address);
}
arrowFunctionWithParam(obj.name, obj.age, obj.address);


const arrowFunctionWithParam2 = name => {
  console.log(name);
}

arrowFunctionWithParam2(obj.name);


/* for each  */
const frutes = [1, 3, 6, 7, 89, 8];
let friute=frutes.forEach((element,index )=> {
  console.log(element+" "+index);
});
console.log(friute);




/* map */
const singleFrute = frutes.map((value, index) => {
  console.log(value + " " + index);
});

console.log(singleFrute);





const people = [
  { id: 1, name: "lablu" },
  { id: 2, name: "bablu" },
  { id: 3, name: "shiplu" },
];

/* filter */
let dataOfPeople = people.filter((items) => {
  console.log(items !== 2);
});
console.log(dataOfPeople);



/* spread */

const arr = [1, 2, 3, 4];
const arr2 = [...arr, 5];
console.log(arr2);

const newPerson = {
  ...dataOfPeople,
  email:"nk@mail.com"
}
console.log(newPerson);



/* destructureing */
const profile = {
  name1: 'Mr Programer',
  address: {
    street: "40 main st",
    ctiy: 'Boston'
  },
  hobbies: ['movies', 'music']
};

const { name1 ,address,hobbies} = profile;
console.log(name1);
console.log(address.street);



/* classes */
class Person {
  constructor(name,age) {
    this.name = name;
    this.age = age;
  }
  greet() {
    return `Hello, my name is ${this.name} and I am ${this.age}`; //with template literal
  }
}
const person1 = new Person("Jhon", 33);
const person3 = new Person("Love", 34);
console.log(person1.greet());

/* Sub classes */

class Customer extends Person{
  constructor(name,age,balance) {
    super(name, age);
    this.balance = balance;
  }
  info() {
    return `${this.name} own ${this.balance}.00`
  }
}
const customer1 = new Customer("mr noughty",32,300);
console.log(customer1.info());



/* module */

//I have problem  in module section

function myModule() {
  this.hello = function () {
    return 'I want to say hello!';
  }

  this.goodbye = function () {
    return 'I want to say goodbye!';
  }
}

module.exports = myModule;





/* promise */


var mypromise1 = new Promise(function (resolve, reject) {
  setTimeout(function () {
    resolve('Yeah Resolved !');
  }, 300);
});

mypromise1.then(function (value) {
  console.log(value);
  
});

console.log(mypromise1);


